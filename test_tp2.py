from tp2 import *

def test_box_create():
    b=Box()
    
def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "truc3" not in b

def test_box_delete():
    b = Box()
    b.add("truc42")
    assert "truc42" in b
    b.delete("truc42")
    assert "truc42" not in b

def test_open():
    b = Box()
    b.openBox()
    assert b.is_open()
    b.closeBox()
    assert not b.is_open()
    

def test_look():
    b=Box()
    b.add("un truc cool")
    assert b.look()=="La boîte contient: un truc cool, "
    b.closeBox()
    assert b.look()=="La boîte est fermée."

def test_thing():
    t=Thing(3)
    assert (t.getVolume()==3)

def test_capacite_boite():
    b=Box()
    b.setCapacite(5)
    assert b.getCapacite()==5
    t1=Thing(3)
    assert b.hasRoomFor(t1)
    t2=Thing(6)
    assert not b.hasRoomFor(t2)
    b2=Box()
    t3=Thing(123456789)
    assert b2.hasRoomFor(t3)

def test_ObjetDansBoite():
    b=Box()
    b.setCapacite(6)
    t=Thing(2)
    b.add(t)
    assert b.getCapacite()==4
    
def test_capacite_add():
    b=Box()
    b.setCapacite(15)
    t1=Thing(5)
    t2=Thing(17)
    assert b.add(t1)
    assert not b.add(t2)
    b.closeBox()
    assert not b.add(t1)
    