class Box(object):
    
    def __init__(self):
        self._contents = []
        self._ouvert= True
        self.capacite=None
        
    def add(self,truc):
        if self.hasRoomFor(truc) and self.is_open():
            self._contents.append(truc)
            if self.getCapacite()!=None:
                self.setCapacite(self.getCapacite()-truc.getVolume())
            return True
        else:
            return False
    
    def delete(self,truc):
        self._contents.remove(truc)
        
    def __contains__(self,machin):
        return machin in self._contents
    
    def is_open(self):
        return self._ouvert
    
    def openBox(self):
        self._ouvert=True
        
    def closeBox(self):
        self._ouvert=False
        
    def look(self):
        if self.is_open():
            res="La boîte contient: "
            for elem in self._contents:
                res=res+elem+", "
            return res
        else:
            return "La boîte est fermée."
    
    def setCapacite(self,cap):
        self.capacite=cap 
    
    def getCapacite(self):
        return self.capacite
    
    def hasRoomFor(self,obj):
        if self.getCapacite()==None:
            return True
        else:        
            return obj.getVolume()<=self.getCapacite()
        

class Thing(object):
    def __init__(self,volume):
        self._volume=volume
        
    def getVolume(self):
        return self._volume
    